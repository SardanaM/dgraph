package main

import (
	repositories "RestAPI/internal/database/repositories"
	services "RestAPI/internal/services"
	handlers "RestAPI/internal/transport/rest/handlers"
	routes "RestAPI/internal/transport/routes"
	//"RestAPI/services"
	"github.com/dgraph-io/dgo/v210/protos/api"
	//  "сontext"
	"github.com/dgraph-io/dgo/v210"
	"google.golang.org/grpc"
	"log"
	"net/http"
)

const dgraphAddress = "127.0.0.1:9000"

func main() {

	//Установка соединения с сервером Dgraph по gRPC
	conn, err := grpc.Dial(dgraphAddress, grpc.WithInsecure())
	if err != nil {
		log.Fatal("Ошибка подключения к Dgraph:", err)
	}
	defer conn.Close()

	dg := dgo.NewDgraphClient(api.NewDgraphClient(conn))

	//Создание репозитория и сервиса для книг
	booksRepo := &repositories.BooksRepository{Dg: dg}
	booksService := &services.BooksService{Repo: booksRepo}

	// Создание обработчика для книг
	booksHandler := &handlers.BooksHandler{BooksService: booksService}

	// Настройка маршрутов
	routes.RoutesBooks(booksHandler)

	//	Создание репозитория и сервиса для электроники
	electronicsRepo := &repositories.ElectronicsRepository{Dg: dg}
	electronicsService := &services.ElectronicsService{Repo: electronicsRepo}

	// Создание обработчика для электроники
	electronicsHandler := &handlers.ElectronicsHandler{ElectronicsService: electronicsService}

	// Настройка маршрутов
	routes.RoutesElectronics(electronicsHandler)

	// Создание репозитория и сервиса для категорий
	categoriesRepo := &repositories.CategoriesRepository{Dg: dg}
	categoriesService := &services.CategoriesService{Repo: categoriesRepo}

	// Создание обработчика для категорий
	categoriesHandler := &handlers.CategoriesHandler{CategoriesService: categoriesService}

	// Настройка маршрутов
	routes.RoutesCategories(categoriesHandler)

	// Создание репозитория и сервиса для медицинских товаров
	medicineRepo := &repositories.MedicineRepository{Dg: dg}
	medicineService := &services.MedicineService{Repo: medicineRepo}

	// Создание обработчика для категорий
	medicineHandler := &handlers.MedicineHandler{MedicineService: medicineService}

	// Настройка маршрутов
	routes.RoutesMedicine(medicineHandler)

	// Создание репозитория и сервиса для категорий
	cosmeticsRepo := &repositories.CosmeticsRepository{Dg: dg}
	cosmeticsService := &services.CosmeticsService{Repo: cosmeticsRepo}

	// Создание обработчика для категорий
	cosmeticsHandler := &handlers.CosmeticsHandler{CosmeticsService: cosmeticsService}

	// Настройка маршрутов
	routes.RoutesCosmetics(cosmeticsHandler)

	// Создание репозитория и сервиса для одежды
	clothesRepo := &repositories.ClothesRepository{Dg: dg}
	clothesService := &services.ClothesService{Repo: clothesRepo}

	// Создание обработчика для категорий
	clothesHandler := &handlers.ClothesHandler{ClothesService: clothesService}

	// Настройка маршрутов
	routes.RoutesClothes(clothesHandler)
	routes.RoutesClothesKind(clothesHandler)

	// Создание репозитория и сервиса для одежды
	metricsRepo := &repositories.MetricsRepository{Dg: dg}
	metricsService := &services.MetricsService{Repo: metricsRepo}

	// Создание обработчика для категорий
	metricsHandler := &handlers.MetricsHandler{MetricsService: metricsService}

	// Настройка маршрутов
	routes.SetupRoutesMetrics(metricsHandler)

	// Создание репозитория и сервиса для категорий
	nodesRepo := &repositories.NodesRepository{Dg: dg}
	nodesService := &services.NodesService{Repo: nodesRepo}

	// Создание обработчика для категорий
	nodesHandler := &handlers.NodesHandler{NodesService: nodesService}

	// Настройка маршрутов
	routes.RoutesNodes1(nodesHandler)
	routes.RoutesNodes2(nodesHandler)
	routes.RoutesNodes3(nodesHandler)
	routes.RoutesNodes4(nodesHandler)
	routes.RoutesNodes5(nodesHandler)

	// Запуск HTTP сервера
	log.Println("Starting server on port :8070")
	if err := http.ListenAndServe(":8070", nil); err != nil {
		log.Fatalf("Failed to start server: %v", err)
	}
}
