package handlers

import (
	"RestAPI/internal/services"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
)

// Обработчик для косметики
type CosmeticsHandler struct {
	CosmeticsService *services.CosmeticsService
}

// Метод обработки запроса на получение всей косметики
func (handler *CosmeticsHandler) GetAllCosmetics(w http.ResponseWriter, r *http.Request) {
	// Получение электроники определенной категории через сервис
	categoryName := r.URL.Query().Get("category_name")
	fmt.Println("categoryName parameter:", categoryName)
	// Вы можете использовать categoryName как строку напрямую, без преобразований
	if categoryName == "" {
		http.Error(w, "Category name is required", http.StatusBadRequest)
		return
	}

	offset := r.URL.Query().Get("offset")
	fmt.Println("Offset parameter:", offset)
	offsetNumber, err := strconv.Atoi(offset)
	if err != nil {
		http.Error(w, "Invalid offset number", http.StatusBadRequest)
		return
	}

	page := r.URL.Query().Get("page")
	fmt.Println("Page parameter:", page)
	pageNumber, err := strconv.Atoi(page)
	if err != nil {
		http.Error(w, "Invalid page number", http.StatusBadRequest)
		return
	}

	resp, err := handler.CosmeticsService.GetAllCosmetics(categoryName, offsetNumber, pageNumber)
	if err != nil {
		http.Error(w, "Сервер error", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(resp); err != nil {
		http.Error(w, "Failed to encode response", http.StatusInternalServerError)
		log.Printf("Failed to encode response: %v\n", err)
		return
	}
}
