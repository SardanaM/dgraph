package handlers

import (
	"RestAPI/internal/services"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
)

// Обработчик для одежды
type ClothesHandler struct {
	ClothesService *services.ClothesService
}

// Метод обработки запроса на получение всей одежды
func (handler *ClothesHandler) GetClothes(w http.ResponseWriter, r *http.Request) {
	// Получение электроники определенной категории через сервис
	categoryName := r.URL.Query().Get("category_name")
	fmt.Println("categoryName parameter:", categoryName)
	if categoryName == "" {
		http.Error(w, "Category name is required", http.StatusBadRequest)
		return
	}

	offset := r.URL.Query().Get("offset")
	fmt.Println("Offset parameter:", offset)
	offsetNumber, err := strconv.Atoi(offset)
	if err != nil {
		http.Error(w, "Invalid offset number", http.StatusBadRequest)
		return
	}

	page := r.URL.Query().Get("page")
	fmt.Println("Page parameter:", page)
	pageNumber, err := strconv.Atoi(page)
	if err != nil {
		http.Error(w, "Invalid page number", http.StatusBadRequest)
		return
	}

	loadAll := r.URL.Query().Get("load_all")
	fmt.Println("Load all parameter:", loadAll)

	// Извлекаем значение load_all из URL-запроса и проверяем его
	var loadAllBool bool
	switch loadAll {
	case "true":
		loadAllBool = true
	case "false":
		loadAllBool = false
	default:
		http.Error(w, "Invalid load_all parameter", http.StatusBadRequest)
		return
	}

	resp, err := handler.ClothesService.GetClothes(categoryName, offsetNumber, pageNumber, loadAllBool)
	if err != nil {
		http.Error(w, "Сервер error", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(resp); err != nil {
		http.Error(w, "Failed to encode response", http.StatusInternalServerError)
		log.Printf("Failed to encode response: %v\n", err)
		return
	}
}

// Метод обработки запроса на получение детской одежды
func (handler *ClothesHandler) GetClothesKind(w http.ResponseWriter, r *http.Request) {
	resp, err := handler.ClothesService.GetClothesKind()
	if err != nil {
		http.Error(w, "Сервер error", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(resp); err != nil {
		http.Error(w, "Failed to encode response", http.StatusInternalServerError)
		log.Printf("Failed to encode response: %v\n", err)
		return
	}
}
