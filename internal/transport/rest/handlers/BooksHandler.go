package handlers

import (
	"RestAPI/internal/services"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
)

// Обработчик для книг
type BooksHandler struct {
	BooksService *services.BooksService
}

// Метод обрабатки запроса на получение списка книг и их характеристик
func (handler *BooksHandler) GetAllBooks(w http.ResponseWriter, r *http.Request) {

	offset := r.URL.Query().Get("offset")
	fmt.Println("Offset parameter:", offset)
	offsetNumber, err := strconv.Atoi(offset)
	if err != nil {
		http.Error(w, "Invalid offset number", http.StatusBadRequest)
		return
	}

	page := r.URL.Query().Get("page")
	fmt.Println("Page parameter:", page)
	pageNumber, err := strconv.Atoi(page)
	if err != nil {
		http.Error(w, "Invalid page number", http.StatusBadRequest)
		return
	}

	resp, err := handler.BooksService.GetAllBooks(pageNumber, offsetNumber)
	if err != nil {
		http.Error(w, "Сервер error", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(resp); err != nil {
		http.Error(w, "Failed to encode response", http.StatusInternalServerError)
		log.Printf("Failed to encode response: %v\n", err)
		return
	}

}
