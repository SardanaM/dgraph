package handlers

import (
	"RestAPI/internal/services"
	"encoding/json"
	"log"
	"net/http"
)

// Обработчик для категорий
type CategoriesHandler struct {
	CategoriesService *services.CategoriesService
}

// Метод обработки запроса на получение всех категорий
func (handler *CategoriesHandler) GetAllCategories(w http.ResponseWriter, r *http.Request) {

	resp, err := handler.CategoriesService.GetAllCategories()
	if err != nil {
		log.Printf("Error getting all categories: %v\n", err)
		http.Error(w, "Сервер error", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(resp); err != nil {
		http.Error(w, "Failed to encode response", http.StatusInternalServerError)
		log.Printf("Failed to encode response: %v\n", err)
		return
	}

}
