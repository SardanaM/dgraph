package handlers

import (
	"RestAPI/internal/services"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

// Обработчик для метрик
type MetricsHandler struct {
	MetricsService *services.MetricsService
}

// Метод обработки запроса на получение всех метрик
func (handler *MetricsHandler) GetAllMetrics(w http.ResponseWriter, r *http.Request) {

	metricName := r.URL.Query().Get("metric_name")
	fmt.Println("categoryName parameter:", metricName)

	if metricName == "" {
		http.Error(w, "Category name is required", http.StatusBadRequest)
		return
	}

	resp, err := handler.MetricsService.GetAllMetrics(metricName)
	if err != nil {
		http.Error(w, "Сервер error", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(resp); err != nil {
		http.Error(w, "Failed to encode response", http.StatusInternalServerError)
		log.Printf("Failed to encode response: %v\n", err)
		return
	}
}
