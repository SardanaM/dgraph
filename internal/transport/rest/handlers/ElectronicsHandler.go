package handlers

import (
	"RestAPI/internal/services"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

// Обработчик для электроники
type ElectronicsHandler struct {
	ElectronicsService *services.ElectronicsService
}

// Метод обработки запроса на получение электроники определенной категории
func (handler *ElectronicsHandler) GetAllElectronics(w http.ResponseWriter, r *http.Request) {
	// Получение электроники определенной категории через сервис
	categoryName := r.URL.Query().Get("category_name")
	fmt.Println("categoryName parameter:", categoryName)
	// Вы можете использовать categoryName как строку напрямую, без преобразований
	if categoryName == "" {
		http.Error(w, "Category name is required", http.StatusBadRequest)
		return
	}

	resp, err := handler.ElectronicsService.GetAllElectronics(categoryName)
	if err != nil {
		http.Error(w, "Сервер error", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(resp); err != nil {
		http.Error(w, "Failed to encode response", http.StatusInternalServerError)
		log.Printf("Failed to encode response: %v\n", err)
		return
	}

}
