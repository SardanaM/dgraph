package handlers

import (
	"RestAPI/internal/services"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
)

// Обработчик для графов
type NodesHandler struct {
	NodesService *services.NodesService
}

// Получения графа определенной модели по названию
func (handler *NodesHandler) GetNodByName(w http.ResponseWriter, r *http.Request) {

	name := r.URL.Query().Get("name")
	fmt.Println("name parameter:", name)

	if name == "" {
		http.Error(w, "name is required", http.StatusBadRequest)
		return
	}

	nameField := r.URL.Query().Get("name_field")
	fmt.Println("name_Field parameter:", nameField)
	if nameField == "" {
		http.Error(w, "name_Field is required", http.StatusBadRequest)
		return
	}

	recurseDepth := r.URL.Query().Get("recurse_depth")
	fmt.Println("recurse_Depth parameter:", recurseDepth)
	recurseDepthNumber, err := strconv.Atoi(recurseDepth)
	if err != nil {
		http.Error(w, "Invalid recurse_Depth parameter", http.StatusBadRequest)
		return
	}

	functionName := r.URL.Query().Get("function_name")
	fmt.Println("functionName parameter:", functionName)
	switch functionName {
	case "eq":
		functionName = "eq"
	case "allofterms":
		functionName = "allofterms"
	default:
		functionName = "allofterms"
		return
	}

	if functionName != "eq" && functionName != "allofterms" {
		http.Error(w, "Invalid search_Function value. Available values: eq, allofterms", http.StatusBadRequest)
		return
	}
	// Проверяем, что функция поиска валидна

	resp, err := handler.NodesService.GetNodByName(name, nameField, recurseDepthNumber, functionName)
	if err != nil {
		http.Error(w, "Сервер error", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(resp); err != nil {
		http.Error(w, "Failed to encode response", http.StatusInternalServerError)
		log.Printf("Failed to encode response: %v\n", err)
		return
	}
}

// Получение всех моделей категории
func (handler *NodesHandler) GetAllNodesByCategory(w http.ResponseWriter, r *http.Request) {

	categoryName := r.URL.Query().Get("category_name")
	fmt.Println("name parameter:", categoryName)

	if categoryName == "" {
		http.Error(w, "name is required", http.StatusBadRequest)
		return
	}

	var nameOnlyBool bool
	switch nameOnlyBool {
	case true:
		nameOnlyBool = true
	case false:
		nameOnlyBool = false
	default:
		nameOnlyBool = false
		//	http.Error(w, "Invalid load_all parameter", http.StatusBadRequest)
		return
	}

	nameField := r.URL.Query().Get("name_field")
	fmt.Println("name_Field parameter:", nameField)
	if nameField == "" {
		http.Error(w, "name_Field is required", http.StatusBadRequest)
		return
	}

	offset := r.URL.Query().Get("offset")
	fmt.Println("Offset parameter:", offset)
	offsetNumber, err := strconv.Atoi(offset)
	if err != nil {
		http.Error(w, "Invalid offset number", http.StatusBadRequest)
		return
	}

	page := r.URL.Query().Get("page")
	fmt.Println("Page parameter:", page)
	pageNumber, err := strconv.Atoi(page)
	if err != nil {
		http.Error(w, "Invalid page number", http.StatusBadRequest)
		return
	}

	recurseDepth := r.URL.Query().Get("recurse_depth")
	fmt.Println("recurse_Depth parameter:", recurseDepth)
	recurseDepthNumber, err := strconv.Atoi(recurseDepth)
	if err != nil {
		http.Error(w, "Invalid recurse_Depth parameter", http.StatusBadRequest)
		return
	}

	order := r.URL.Query().Get("order")
	fmt.Println("order parameter:", order)
	switch order {
	case "orderasc":
		order = "orderasc"
	case "orderdesc":
		order = "orderdesc"
	default:
		order = "orderasc"
		return
	}

	orderCriteria := r.URL.Query().Get("order_criteria")
	fmt.Println("orderCriteria parameter:", orderCriteria)
	if orderCriteria == "" {
		http.Error(w, "orderCriteria is required", http.StatusBadRequest)
		return
	}

	// Извлекаем значение load_all из URL-запроса и проверяем его
	var loadAllBool bool
	switch loadAllBool {
	case true:
		loadAllBool = true
	case false:
		loadAllBool = false
	default:
		http.Error(w, "Invalid load_all parameter", http.StatusBadRequest)
		return
	}

	resp, err := handler.NodesService.GetAllNodesByCategory(categoryName, nameOnlyBool, nameField, offsetNumber, pageNumber, recurseDepthNumber, order, orderCriteria, loadAllBool)
	if err != nil {
		http.Error(w, "Сервер error", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(resp); err != nil {
		http.Error(w, "Failed to encode response", http.StatusInternalServerError)
		log.Printf("Failed to encode response: %v\n", err)
		return
	}
}

// Получение графов продуктов с определенными характеристиками. Работает только для графа с шириной, не более 2
func (handler *NodesHandler) GetNodesByFilters(w http.ResponseWriter, r *http.Request) {

	dgraphTypeName := r.URL.Query().Get("dgraph_type_name")
	fmt.Println("dgraphTypeName parameter:", dgraphTypeName)

	if dgraphTypeName == "" {
		http.Error(w, "dgraphTypeNameis required", http.StatusBadRequest)
		return
	}

	var predicates map[string]string
	if err := json.NewDecoder(r.Body).Decode(&predicates); err != nil {
		http.Error(w, "Failed to parse request body", http.StatusBadRequest)
		return
	}

	// Вывод параметров predicates для отладки
	fmt.Println("Predicates:", predicates)

	edgeName := r.URL.Query().Get("edge_name")
	fmt.Println("edgeName parameter:", edgeName)

	if edgeName == "" {
		http.Error(w, "edgeName is required", http.StatusBadRequest)
		return
	}

	offset := r.URL.Query().Get("offset")
	fmt.Println("Offset parameter:", offset)
	offsetNumber, err := strconv.Atoi(offset)
	if err != nil {
		http.Error(w, "Invalid offset number", http.StatusBadRequest)
		return
	}

	page := r.URL.Query().Get("page")
	fmt.Println("Page parameter:", page)
	pageNumber, err := strconv.Atoi(page)
	if err != nil {
		http.Error(w, "Invalid page number", http.StatusBadRequest)
		return
	}

	resp, err := handler.NodesService.GetNodesByFilters(dgraphTypeName, predicates, edgeName, offsetNumber, pageNumber)
	if err != nil {
		http.Error(w, "Сервер error", http.StatusInternalServerError)
		return
	}

	// Вывод параметров resp для отладки
	fmt.Println("Resp:", resp)

	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(resp); err != nil {
		http.Error(w, "Failed to encode response", http.StatusInternalServerError)
		log.Printf("Failed to encode response: %v\n", err)
		return
	}

}

// Получение графов продуктов с определенной категории и бренда.
func (handler *NodesHandler) GetNodesOfCategoryByBrand(w http.ResponseWriter, r *http.Request) {

	categoryName := r.URL.Query().Get("category_name")
	fmt.Println("categoryName parameter:", categoryName)
	if categoryName == "" {
		http.Error(w, "Category name is required", http.StatusBadRequest)
		return
	}

	offset := r.URL.Query().Get("offset")
	fmt.Println("Offset parameter:", offset)
	offsetNumber, err := strconv.Atoi(offset)
	if err != nil {
		http.Error(w, "Invalid offset number", http.StatusBadRequest)
		return
	}

	page := r.URL.Query().Get("page")
	fmt.Println("Page parameter:", page)
	pageNumber, err := strconv.Atoi(page)
	if err != nil {
		http.Error(w, "Invalid page number", http.StatusBadRequest)
		return
	}

	brandName := r.URL.Query().Get("brand_name")
	fmt.Println("brandName parameter:", brandName)
	if brandName == "" {
		http.Error(w, "Brand name is required", http.StatusBadRequest)
		return
	}

	resp, err := handler.NodesService.GetNodesOfCategoryByBrand(categoryName, brandName, offsetNumber, pageNumber)
	if err != nil {
		http.Error(w, "Сервер error", http.StatusInternalServerError)
		return
	}

	fmt.Println("Resp:", resp)

	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(resp); err != nil {
		http.Error(w, "Failed to encode response", http.StatusInternalServerError)
		log.Printf("Failed to encode response: %v\n", err)
		return
	}
}

// Получение графов продуктов определенного бренда.
func (handler *NodesHandler) GetNodesByBrand(w http.ResponseWriter, r *http.Request) {

	brandName := r.URL.Query().Get("brand_name")
	fmt.Println("brandName parameter:", brandName)
	if brandName == "" {
		http.Error(w, "Brand name is required", http.StatusBadRequest)
		return
	}

	offset := r.URL.Query().Get("offset")
	fmt.Println("Offset parameter:", offset)
	offsetNumber, err := strconv.Atoi(offset)
	if err != nil {
		http.Error(w, "Invalid offset number", http.StatusBadRequest)
		return
	}

	page := r.URL.Query().Get("page")
	fmt.Println("Page parameter:", page)
	pageNumber, err := strconv.Atoi(page)
	if err != nil {
		http.Error(w, "Invalid page number", http.StatusBadRequest)
		return
	}

	resp, err := handler.NodesService.GetNodesByBrand(brandName, offsetNumber, pageNumber)
	if err != nil {
		http.Error(w, "Сервер error", http.StatusInternalServerError)
		return
	}

	fmt.Println("Resp:", resp)

	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(resp); err != nil {
		http.Error(w, "Failed to encode response", http.StatusInternalServerError)
		log.Printf("Failed to encode response: %v\n", err)
		return
	}

}
