package routes

import (
	"RestAPI/internal/transport/rest/handlers"
	"net/http"
)

// Функция маршрутизации для детской одежды
func RoutesClothesKind(clothesHandler *handlers.ClothesHandler) {
	http.HandleFunc("/api/v1/clothes/kind", clothesHandler.GetClothesKind)
}
