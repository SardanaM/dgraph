package routes

import (
	"RestAPI/internal/transport/rest/handlers"
	"net/http"
)

// Функция маршрутизации для категорий
func RoutesCategories(categoriesHandler *handlers.CategoriesHandler) {
	http.HandleFunc("/api/v1/categories", categoriesHandler.GetAllCategories)
}
