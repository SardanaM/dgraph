package routes

import (
	"RestAPI/internal/transport/rest/handlers"
	"net/http"
)

// Функция маршрутизации для получения графа определенной модели по названию
func RoutesNodes1(nodesHandler *handlers.NodesHandler) {
	http.HandleFunc("/api/v1/nodes", nodesHandler.GetNodByName)
}

// Функция маршрутизации для получения всех моделей категории
func RoutesNodes2(nodesHandler *handlers.NodesHandler) {
	http.HandleFunc("/api/v1/nodes/category", nodesHandler.GetAllNodesByCategory)
}

// Функция маршрутизации для получения графов продуктов с определенными характеристиками. Работает только для графа с шириной, не более 2
func RoutesNodes3(nodesHandler *handlers.NodesHandler) {
	http.HandleFunc("/api/v1/nodes/filters/category", nodesHandler.GetNodesByFilters)
}

// Функция маршрутизации для получения графов продуктов с определенной категории и бренда
func RoutesNodes4(nodesHandler *handlers.NodesHandler) {
	http.HandleFunc("/api/v1/filters/category_and_brand", nodesHandler.GetNodesOfCategoryByBrand)
}

// Функция маршрутизации для получения графов продуктов определенного бренда
func RoutesNodes5(nodesHandler *handlers.NodesHandler) {
	http.HandleFunc("/api/v1/nodes/filters/brand", nodesHandler.GetNodesByBrand)
}
