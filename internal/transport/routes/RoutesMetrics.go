package routes

import (
	"RestAPI/internal/transport/rest/handlers"
	"net/http"
)

// Функция маршрутизации для метрик
func SetupRoutesMetrics(metricsHandler *handlers.MetricsHandler) {
	http.HandleFunc("/api/v1/metrics", metricsHandler.GetAllMetrics)
}
