package routes

import (
	"RestAPI/internal/transport/rest/handlers"
	"net/http"
)

// Функция маршрутизации для книг
func RoutesBooks(bookHandler *handlers.BooksHandler) {
	http.HandleFunc("/api/v1/books", bookHandler.GetAllBooks)
}
