package routes

import (
	"RestAPI/internal/transport/rest/handlers"
	"net/http"
)

// Функция маршрутизации для электроники
func RoutesElectronics(electronicHandler *handlers.ElectronicsHandler) {
	http.HandleFunc("/api/v1/electronics", electronicHandler.GetAllElectronics)
}
