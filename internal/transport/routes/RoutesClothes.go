package routes

import (
	"RestAPI/internal/transport/rest/handlers"
	"net/http"
)

// Функция маршрутизации для одежды
func RoutesClothes(clothesHandler *handlers.ClothesHandler) {
	http.HandleFunc("/api/v1/clothes", clothesHandler.GetClothes)
}
