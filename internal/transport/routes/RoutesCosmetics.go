package routes

import (
	"RestAPI/internal/transport/rest/handlers"
	"net/http"
)

// Функция маршрутизации для косметики
func RoutesCosmetics(cosmeticsHandler *handlers.CosmeticsHandler) {
	http.HandleFunc("/api/v1/cosmetics", cosmeticsHandler.GetAllCosmetics)
}
