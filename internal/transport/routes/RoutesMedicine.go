package routes

import (
	"RestAPI/internal/transport/rest/handlers"
	"net/http"
)

// Функция маршрутизации для медицины
func RoutesMedicine(medicineHandler *handlers.MedicineHandler) {
	http.HandleFunc("/api/v1/medicine", medicineHandler.GetAllMedicine)
}
