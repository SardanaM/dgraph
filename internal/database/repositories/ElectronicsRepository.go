package repositories

import (
	"RestAPI/internal/models"
	"context"
	"encoding/json"
	"fmt"
	"github.com/dgraph-io/dgo/v210"
	"log"
)

type ElectronicsRepository struct {
	Dg *dgo.Dgraph // Клиент Dgraph
}

func (r ElectronicsRepository) GetAllElectronics(categoryName string) ([]map[string]interface{}, error) {
	// Создание запроса к базе данных
	queryText := `
        query categories($name: string) {
            cat(func: type("Категория")) @filter(eq(name, $name)) {
                products as <~категория>{}
            }
            query(func: uid(products), first: 10, orderasc: <название>) @normalize {
                model : <модель>
                <бренд> {
                    brand: name
                }
                <категория> {
                    subject: name
                }
            }
        }
    `

	resp, err := r.Dg.NewReadOnlyTxn().QueryWithVars(context.TODO(), queryText, map[string]string{
		"$name": categoryName,
	})

	if err != nil {
		log.Println("error querying data:", err)
		return nil, fmt.Errorf("failed to query data: %v", err)
	}

	parsedData := models.ParsedData{Query: make([]map[string]interface{}, 0)}

	if err := json.Unmarshal(resp.Json, &parsedData); err != nil {
		log.Println("error parsing JSON:", err)
		return nil, fmt.Errorf("failed to parse JSON: %v", err)
	}

	return parsedData.Query, nil

}
