package repositories

import (
	"RestAPI/internal/models"
	"context"
	"encoding/json"
	"fmt"
	"github.com/dgraph-io/dgo/v210"
	"log"
)

type MetricsRepository struct {
	Dg *dgo.Dgraph // Клиент Dgraph
}

// Метод гет - Получение количества метрик
func (r MetricsRepository) GetAllMetrics(metricName string) ([]map[string]interface{}, error) {
	// Создание запроса к базе данных
	queryText := `
      query metrics($name: string) {
      query(func: eq(name, $name)) {
        <количество>: count(<~категория>)
     }
   }
 
`
	resp, err := r.Dg.NewReadOnlyTxn().QueryWithVars(context.TODO(), queryText, map[string]string{
		"$name": metricName,
	})

	if err != nil {
		log.Println("error querying data:", err)
		return nil, fmt.Errorf("failed to query data: %v", err)
	}

	parsedData := models.ParsedData{Query: make([]map[string]interface{}, 0)}

	if err := json.Unmarshal(resp.Json, &parsedData); err != nil {
		log.Println("error parsing JSON:", err)
		return nil, fmt.Errorf("failed to parse JSON: %v", err)
	}

	return parsedData.Query, nil
}
