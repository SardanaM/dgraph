package repositories

import (
	"RestAPI/internal/models"
	"context"
	"encoding/json"
	"fmt"
	"github.com/dgraph-io/dgo/v210"
	"log"
	"strconv"
	"strings"
)

type NodesRepository struct {
	Dg *dgo.Dgraph // Клиент Dgraph
}

// Метод гет
func (r NodesRepository) GetNodByName(name string, nameField string, recurseDepth int, functionName string) ([]map[string]interface{}, error) {
	// Создание запроса к базе данных
	queryText := fmt.Sprintf(`
		query node($name: string) {
			query(func: %s(<%s>, $name))
			@recurse(depth: %d) {
				expand(_all_)
			}
		}
	`, functionName, nameField, recurseDepth) // Заменяем только первое вхождение %s

	//	Продолжаем замену для остальных параметров
	queryText = strings.Replace(queryText, "%s", nameField, 1)
	queryText = strings.Replace(queryText, "%d", strconv.Itoa(recurseDepth), 1)

	resp, err := r.Dg.NewReadOnlyTxn().QueryWithVars(context.TODO(), queryText, map[string]string{
		"$name": name,
	})

	if err != nil {
		log.Println("error querying data:", err)
		return nil, fmt.Errorf("failed to query data: %v", err)
	}

	parsedNodes := models.ParsedData{Query: make([]map[string]interface{}, 0)}

	if err := json.Unmarshal(resp.Json, &parsedNodes); err != nil {
		log.Println("error parsing JSON:", err)
		return nil, fmt.Errorf("failed to parse JSON: %v", err)
	}

	return parsedNodes.Query, nil

}

func (r NodesRepository) GetAllNodesByCategory(categoryName string, nameOnly bool, nameField string, offset int, page int,
	recurseDepth int, order string, orderCriteria string, loadAll bool) ([]map[string]interface{}, error) {

	var ordering string
	if orderCriteria != "" {
		ordering = fmt.Sprintf(", %s: <%s>", order, orderCriteria)
	}
	// Создание запроса к базе данных
	queryCountText := fmt.Sprintf(`
        query metrics($name: string) {
          query(func: eq(name, $name)) {
            <количество>: count(<~категория>)
         }
       }
		`)

	resp, err := r.Dg.NewReadOnlyTxn().QueryWithVars(context.TODO(), queryCountText, map[string]string{
		"$name": categoryName,
	})

	if err != nil {
		log.Println("error querying data:", err)
		return nil, fmt.Errorf("failed to query data: %v", err)
	}

	var modelsCount int
	if len(resp.Json) > 0 {
		var parsedResponse struct {
			Query []map[string]interface{} `json:"query"`
		}
		if err := json.Unmarshal(resp.Json, &parsedResponse); err != nil {
			log.Println("error parsing JSON:", err)
			return nil, fmt.Errorf("failed to parse JSON: %v", err)
		}
	}

	var fieldsToShow string
	var queryText string

	if nameOnly {
		fieldsToShow = fmt.Sprintf("<%s>", nameField)
		// Создание запроса к базе данных
		queryText = fmt.Sprintf(`
			query categories($name: string){
				cat(func: type("Категория")) @filter(eq(name, $name)) {
					products as  <~категория>{}
				}
				query(func: uid(products), first: %d%s){
					%s
				}
			}
		`, modelsCount, ordering, fieldsToShow)
	} else {
		fieldsToShow = "expand(_all_)"
		// Создание запроса к базе данных
		queryText = fmt.Sprintf(`
			query categories($name: string){
				cat(func: type("Категория")) @filter(eq(name, $name)) {
					products as  <~категория>{}
				}
				query(func: uid(products)%s, first: %d, offset: %d) @recurse(depth: %d){
					%s
				}
			}
		`, ordering, offset, page, recurseDepth, fieldsToShow)
	}

	resp1, err := r.Dg.NewReadOnlyTxn().QueryWithVars(context.TODO(), queryText, map[string]string{
		"$name": categoryName,
	})

	if err != nil {
		log.Println("error querying data:", err)
		return nil, fmt.Errorf("failed to query data: %v", err)
	}

	parsedNodes := models.ParsedData{Query: make([]map[string]interface{}, 0)}

	if err := json.Unmarshal(resp1.Json, &parsedNodes); err != nil {
		log.Println("error parsing JSON:", err)
		return nil, fmt.Errorf("failed to parse JSON: %v", err)
	}

	return parsedNodes.Query, nil

}

func (r NodesRepository) GetNodesByFilters(dgraphTypeName string, predicates map[string]string, edgeName string, offset int, page int) (models.ParsedNodesFilters, error) {
	var filters []string
	for filterObject, filterValue := range predicates {
		filters = append(filters, fmt.Sprintf(`eq(%s, "%s")`, filterObject, filterValue))
	}

	filtersString := strings.Join(filters, " and ")
	// Создание запроса к базе данных
	queryText := fmt.Sprintf(`
        query node_type($node_type_name: string){
            query_items (func: type($node_type_name)) @filter(%s) {
                total: count(<~%s>)
                items as <~%s> {}
            }
            query_total(func: uid(items))  @recurse(depth: 2) {
                total: count(uid)
            }
            query(func: uid(items), first: %d, offset: %d) 
            @recurse(depth: 2) {
                expand(_all_)
            }
        }
    `, filtersString, edgeName, edgeName, offset, page)

	resp, err := r.Dg.NewReadOnlyTxn().QueryWithVars(context.TODO(), queryText, map[string]string{
		"$node_type_name": dgraphTypeName,
	})

	fmt.Println("Resp:", resp.Json)

	if err != nil {
		log.Println("error querying data:", err)
		return models.ParsedNodesFilters{}, fmt.Errorf("failed to query data: %v", err)
	}

	var parsedData models.ParsedNodesFilters

	parsedData = models.ParsedNodesFilters{
		Query: make([]map[string]interface{}, 0),
		Total: make([]map[string]interface{}, 0),
	}

	if err := json.Unmarshal(resp.Json, &parsedData); err != nil {
		log.Println("error parsing JSON:", err)
		return models.ParsedNodesFilters{}, fmt.Errorf("failed to parse JSON: %v", err)
	}

	return parsedData, nil

}

func (r NodesRepository) GetNodesOfCategoryByBrand(categoryName string, brandName string, offset int, page int) (models.ParsedNodesFilters, error) {
	// Создание запроса к базе данных
	queryText := fmt.Sprintf(`
	   query categories($category_name: string, $brand_name: string){
	       query_brand(func: type("Бренд")) @filter(eq(name, $brand_name)) {
	           brand_items as <~бренд>{}
	       }
	       query_category(func: type("Категория")) @filter(eq(name, $category_name)){
	           items as <~категория> @filter(uid(brand_items)){}
	       }
	       query_total(func: uid(items))  @recurse(depth: 2) {
	           total: count(uid)
	       }
	       query(func: uid(items), first: %d, offset: %d)
	       @recurse(depth: 2) {
	           expand(_all_)
	       }
	   }
	`, offset, page)

	resp, err := r.Dg.NewReadOnlyTxn().QueryWithVars(context.TODO(), queryText, map[string]string{
		"$category_name": categoryName,
		"$brand_name":    brandName,
	})

	if err != nil {
		log.Println("error querying data:", err)
		return models.ParsedNodesFilters{}, fmt.Errorf("failed to query data: %v", err)
	}

	fmt.Println("Resp:", resp.Json)

	var parsedData models.ParsedNodesFilters

	parsedData = models.ParsedNodesFilters{
		Query: make([]map[string]interface{}, 0),
		Total: make([]map[string]interface{}, 0),
	}

	if err := json.Unmarshal(resp.Json, &parsedData); err != nil {
		log.Println("error parsing JSON:", err)
		return models.ParsedNodesFilters{}, fmt.Errorf("failed to parse JSON: %v", err)
	}

	return parsedData, nil
}

func (r NodesRepository) GetNodesByBrand(brandName string, offset int, page int) (models.ParsedBrands, error) {
	// Создание запроса к базе данных
	queryText := fmt.Sprintf(`
    query brand($name: string){
  query_brand(func: type("Бренд")) @filter(eq(name, $name)) {
        total: count(<~бренд>)
        items as <~бренд> {}
      }
     query(func: uid(items), first: %d, offset: %d) 
     @recurse(depth: 2) {
        expand(_all_)
      }
}
`, offset, page)

	resp, err := r.Dg.NewReadOnlyTxn().QueryWithVars(context.TODO(), queryText, map[string]string{
		"$brand_name": brandName,
	})

	fmt.Println("Resp:", resp.Json)

	if err != nil {
		log.Println("error querying data:", err)
		return models.ParsedBrands{}, fmt.Errorf("failed to query data: %v", err)
	}

	var parsedData models.ParsedBrands

	parsedData = models.ParsedBrands{
		Query: make([]map[string]interface{}, 0),
		Total: make([]map[string]interface{}, 0),
	}

	if err := json.Unmarshal(resp.Json, &parsedData); err != nil {
		log.Println("error parsing JSON:", err)
		return models.ParsedBrands{}, fmt.Errorf("failed to parse JSON: %v", err)
	}

	return parsedData, nil

}
