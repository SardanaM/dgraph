package repositories

import (
	"RestAPI/internal/models"
	"context"
	"encoding/json"
	"fmt"
	"github.com/dgraph-io/dgo/v210"
	"log"
)

type ClothesRepository struct {
	Dg *dgo.Dgraph // Клиент Dgraph
}

// Метод гет - Получение списка одежды и ее характеристик
func (r ClothesRepository) GetAllClothes(categoryName string, offset int, page int, loadAll bool) ([]map[string]interface{}, error) {
	// Создание запроса к базе данных
	queryText := fmt.Sprintf(`
	query name($name: string) {
		query (func: eq(<вид_одежды>, $name),
	first:  %d, offset:  %d) @recurse {
			expand(_all_)
		}
	}
`, offset, page)

	resp, err := r.Dg.NewReadOnlyTxn().QueryWithVars(context.TODO(), queryText, map[string]string{
		"$name": categoryName, //"offset": strconv.Itoa(offset), "page": strconv.Itoa(page),
	})

	if err != nil {
		log.Println("error querying data:", err)
		return nil, fmt.Errorf("failed to query data: %v", err)
	}

	parsedData := models.ParsedData{Query: make([]map[string]interface{}, 0)}

	if err := json.Unmarshal(resp.Json, &parsedData); err != nil {
		log.Println("error parsing JSON:", err)
		return nil, fmt.Errorf("failed to parse JSON: %v", err)
	}

	return parsedData.Query, nil

}

// Метод гет - Получение списка детской одежды и их характеристик
func (r ClothesRepository) GetClothesKind() (models.ClothesKids, error) {
	// Создание запроса к базе данных
	queryText := `
   {
     query(func: type("Одежда")) @groupby(<вид_одежды>) {
       count(uid)
     }
   }
`
	resp, err := r.Dg.NewReadOnlyTxn().QueryWithVars(context.TODO(), queryText, map[string]string{})

	if err != nil {
		log.Println("error querying data:", err)
		return models.ClothesKids{}, fmt.Errorf("failed to query data: %v", err)
	}

	parsedData := models.ClothesKids{
		Query: make([]map[string]interface{}, 0)}

	if err := json.Unmarshal(resp.Json, &parsedData); err != nil {
		log.Println("error parsing JSON:", err)
		return models.ClothesKids{}, fmt.Errorf("failed to parse JSON: %v", err)
	}

	return parsedData, nil

}
