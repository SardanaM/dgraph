package repositories

import "RestAPI/internal/models"

type Repositories interface {
	GetAllBooks(page int, offset int) ([]map[string]interface{}, error)
	GetAllCategories() ([]models.Category, error)
	GetAllClothes(categoryName string, offset int, page int, loadAll bool) ([]map[string]interface{}, error)
	GetClothesKind() (models.ClothesKids, error)
	GetAllCosmetics(name string, offset int, page int) ([]map[string]interface{}, error)
	GetAllElectronics(categoryName string) ([]map[string]interface{}, error)
	GetAllMedicine(page int, offset int) ([]map[string]interface{}, error)
	GetAllMetrics(metricName string) ([]map[string]interface{}, error)
	GetNodByName(name string, nameField string, recurseDepth int, functionName string) ([]map[string]interface{}, error)
	GetAllNodesByCategory(categoryName string, nameOnly bool, nameField string, offset int, page int,
		recurseDepth int, order string, orderCriteria string, loadAll bool) ([]map[string]interface{}, error)
	GetNodesByFilters(dgraphTypeName string, predicates map[string]string, edgeName string, offset int, page int) (models.ParsedNodesFilters, error)
	GetNodesOfCategoryByBrand(categoryName string, brandName string, offset int, page int) (models.ParsedNodesFilters, error)
	GetNodesByBrand(brandName string, offset int, page int) (models.ParsedBrands, error)
}
