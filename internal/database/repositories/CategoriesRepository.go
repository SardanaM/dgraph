package repositories

import (
	"RestAPI/internal/models"
	"context"
	"encoding/json"
	"fmt"
	"github.com/dgraph-io/dgo/v210"
	"log"
)

type CategoriesRepository struct {
	Dg *dgo.Dgraph // Клиент Dgraph
}

// Метод гет - Получение списка категорий и ее характеристик
func (r CategoriesRepository) GetAllCategories() (models.ParsedCategories, error) {
	// Создание запроса к базе данных
	queryText := `
	query categories {
		cat(func: type("Категория")) {		
            uid
			name

		}
	}
	`

	resp, err := r.Dg.NewReadOnlyTxn().QueryWithVars(context.TODO(), queryText, map[string]string{})

	fmt.Println(resp)

	if err != nil {
		log.Println("error querying data:", err)
		return models.ParsedCategories{}, fmt.Errorf("failed to query data: %v", err)
	}

	parsedCategories := models.ParsedCategories{Query: make([]map[string]interface{}, 0)}

	err = json.Unmarshal(resp.Json, &parsedCategories)
	if err != nil {
		log.Println("ошибка при парсинге json", err)
		return models.ParsedCategories{}, fmt.Errorf("%v", "unmarshall error")
	}

	return parsedCategories, nil

}
