package repositories

import (
	"RestAPI/internal/models"
	"context"
	"encoding/json"
	"fmt"
	"github.com/dgraph-io/dgo/v210"
	"log"
)

type BooksRepository struct {
	Dg *dgo.Dgraph // Клиент Dgraph
}

// Метод гет - Получение списка книг и их характеристик
func (r BooksRepository) GetAllBooks(page int, offset int) ([]map[string]interface{}, error) {
	// Создание запроса к базе данных
	queryText := fmt.Sprintf(`
	query categories($name: string) {
		cat(func: type("Категория")) @filter(eq(name, $name)) {
		products as <~категория>{}
		}
		query(func: uid(products), first: %d, offset: %d, orderasc: <название_книги>) {
			expand(_all_)
		}
	}
	`, offset, page)

	resp, err := r.Dg.NewReadOnlyTxn().QueryWithVars(context.TODO(), queryText, map[string]string{
		"$name": "книга",
	})

	if err != nil {
		log.Println("error querying data:", err)
		return nil, fmt.Errorf("failed to query data: %v", err)
	}

	parsedData := models.ParsedData{Query: make([]map[string]interface{}, 0)}

	if err := json.Unmarshal(resp.Json, &parsedData); err != nil {
		log.Println("error parsing JSON:", err)
		return nil, fmt.Errorf("failed to parse JSON: %v", err)
	}

	return parsedData.Query, nil
}
