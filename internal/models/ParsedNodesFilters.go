package models

type ParsedNodesFilters struct {
	Total []map[string]interface{} `json:"query_total"`
	Query []map[string]interface{} `json:"query"`
}
type ParsedNodesFilters2 struct {
	Total    float64                  `json:"total"`
	Page     int                      `json:"page"`
	Products []map[string]interface{} `json:"products"`
}
