package models

type Clothes struct {
	Seasonality []struct {
		SeasonName string `json:"наименование_сезона"`
	} `json:"сезонность_одежды"`
	Material struct {
		MaterialProperties         []string `json:"свойство_материала"`
		MaterialTexture            []string `json:"фактура_материала"`
		DopMaterialCharacteristics []string `json:"доп_характеристики_материала"`
		MaterialName               string   `json:"название_материала"`
		MaterialComponents         []string `json:"компоненты_материала"`
		MaterialCharacteristics    []string `json:"характеристика_материала"`
	} `json:"материал_одежды"`
	Style struct {
		StyleName string `json:"название_фасона"`
		StyleType string `json:"тип_фасона"`
	} `json:"фасон_одежды"`
	Functionality struct {
		FunctionalityType string `json:"тип_функциональности_одежды"`
	} `json:"функциональность_одежды"`
	Gender []struct {
		GenderName string `json:"наименование_пола"`
	} `json:"пол"`
	Category []struct {
		Name string `json:"name"`
	} `json:"категория"`
	Purpose []struct {
		PurposeType    string `json:"тип_назначения"`
		PurposeSubtype string `json:"подтип_назначения,omitempty"`
	} `json:"назначение_одежды"`
	Type string `json:"вид_одежды"`
	Age  struct {
		AgeName string `json:"наименование_возраста"`
	} `json:"возраст"`
	Length struct {
		LengthType string `json:"тип_длины_одежды"`
	} `json:"длина_одежды"`
	MaterialDesign struct {
		PrintType string `json:"тип_принта"`
	} `json:"дизайн_материала_одежды"`
}
