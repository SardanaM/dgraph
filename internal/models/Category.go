package models

type Category struct {
	Uid  string `json:"uid"`
	Name string `json:"name"`
}
