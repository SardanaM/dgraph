package models

type Cosmetics struct {
	Коллекция             string   `json:"коллекция"`
	Зона                  []string `json:"зона"`
	Цвет                  []string `json:"цвет"`
	ТипКожи               []string `json:"тип_кожи"`
	Тон                   []string `json:"тон"`
	Текстура              []string `json:"текстура"`
	Объем                 []string `json:"объем"`
	Эффект                []string `json:"эффект"`
	ОсобенностиСостава    []string `json:"особенности_состава"`
	ОсобенностиТуши       []string `json:"особенности_туши"`
	Особенности           []string `json:"особенности"`
	Преимущества          []string `json:"преимущества"`
	Финиш                 []string `json:"финиш"`
	ВозрастныеОграничения []string `json:"возрастные_ограничения"`
	СтранаПроизводства    string   `json:"страна_производства"`
	ТипКосметики          []string `json:"тип_косметики"`
	ОбщаяКатегория        []string `json:"общая_категория"`
}
