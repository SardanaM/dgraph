package models

type ParsedBrands struct {
	Total []map[string]interface{} `json:"query_brand"`
	Query []map[string]interface{} `json:"query"`
}
type ParsedBrands2 struct {
	Total    float64                  `json:"total"`
	Page     int                      `json:"page"`
	Products []map[string]interface{} `json:"products"`
}
