package models

import "time"

type Metrics struct {
	MetricName  string    `json:"metric_name"`
	MetricValue int       `json:"metric_value"`
	Date        time.Time `json:"date"`
}
