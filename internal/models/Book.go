package models

type Book struct {
	Name                    string   `json:"название_книги"`
	Author                  []string `json:"автор_книги"`
	Age_restrictions        []string `json:"возрастные_ограничения"`
	Languages_of_the_book   []string `json:"языки_книги"`
	The_subject_of_the_book string   `json:"тематика_книги"`
	Description_of_the_book string   `json:"описание_книги"`
}
