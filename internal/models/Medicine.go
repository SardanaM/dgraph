package models

type Medicine struct {
	Name                                       string `json:"наименование"`
	The_name_of_the_anatomical_organ_or_system string `json:"наименование_анатомического_органа_или_системы"`
	The_name_of_the_therapeutic_group          string `json:"наименование_терапевтической_группы"`
	The_name_of_the_pharmacological_group      string `json:"наименование_фармакологической_группы"`
	Chemical_Group_names                       string `json:"наименование_химической_группы"`
	Trade_name                                 string `json:"торговое_название"`
}
