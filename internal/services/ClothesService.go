package services

import (
	"RestAPI/internal/database/repositories"
	"github.com/pkg/errors"
)

// Сервис для элементов
type ClothesService struct {
	Repo *repositories.ClothesRepository
}

// Метод get
func (service *ClothesService) GetClothes(categoryName string, offset int, page int, loadAll bool) ([]map[string]interface{}, error) {
	// Ваша логика для получения всей косметики, например:
	clothes, err := service.Repo.GetAllClothes(categoryName, offset, page, loadAll)
	if err != nil {
		return nil, err
	}
	return clothes, nil
}

// Метод get
func (service *ClothesService) GetClothesKind() ([]string, error) {
	//var resp models.ClothesKids2
	var resp []string
	// Получаем список детской одежды от репозитория, например:
	clothes, err := service.Repo.GetClothesKind()
	if err != nil {
		return resp, err
	}

	if len(clothes.Query) == 0 {
		return resp, errors.New("no clothes kinds found")
	}

	// Обрабатываем результат запроса
	for _, group := range clothes.Query {
		if groupData, ok := group["@groupby"].([]interface{}); ok {
			for _, item := range groupData {
				if groupItem, ok := item.(map[string]interface{}); ok {
					if clothesKind, ok := groupItem["вид_одежды"].(string); ok {
						resp = append(resp, clothesKind)
					}
				}
			}
		}
	}

	return resp, nil

}
