package services

import (
	"RestAPI/internal/database/repositories"
)

// Сервис для элементов
type MetricsService struct {
	Repo *repositories.MetricsRepository
}

// Метод get
func (service *MetricsService) GetAllMetrics(metric_name string) ([]map[string]interface{}, error) {
	//  логика для получения метрик:
	electroniсs, err := service.Repo.GetAllMetrics(metric_name)
	if err != nil {
		return nil, err
	}
	return electroniсs, nil
}
