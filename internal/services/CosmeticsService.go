package services

import (
	"RestAPI/internal/database/repositories"
)

// Сервис для элементов
type CosmeticsService struct {
	Repo *repositories.CosmeticsRepository
}

// Метод get
func (service *CosmeticsService) GetAllCosmetics(name string, offset int, page int) ([]map[string]interface{}, error) {
	// Ваша логика для получения всей косметики, например:
	cosmetics, err := service.Repo.GetAllCosmetics(name, offset, page)
	if err != nil {
		return nil, err
	}
	return cosmetics, nil
}
