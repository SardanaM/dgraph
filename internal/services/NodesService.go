package services

import (
	"RestAPI/internal/database/repositories"
	"RestAPI/internal/models"
)

// Сервис для элементов
type NodesService struct {
	Repo *repositories.NodesRepository
}

// Метод get
func (service *NodesService) GetNodByName(name string, nameField string, recurseDepth int, searchFunction string) ([]map[string]interface{}, error) {

	nodes, err := service.Repo.GetNodByName(name, nameField, recurseDepth, searchFunction)
	if err != nil {
		return nil, err
	}
	return nodes, nil
}

// Метод get
func (service *NodesService) GetAllNodesByCategory(categoryName string, nameOnly bool, nameField string, offset int, page int, recurseDepth int, order string, orderCriteria string, loadAll bool) ([]map[string]interface{}, error) {

	nodes, err := service.Repo.GetAllNodesByCategory(categoryName, nameOnly, nameField, offset, page, recurseDepth, order, orderCriteria, loadAll)
	if err != nil {
		return nil, err
	}
	return nodes, nil
}

// Метод post
func (service *NodesService) GetNodesByFilters(dgraphTypeName string, predicates map[string]string, edgeName string, offset int, page int) (models.ParsedNodesFilters2, error) {
	var resp models.ParsedNodesFilters2

	nodes, err := service.Repo.GetNodesByFilters(dgraphTypeName, predicates, edgeName, offset, page)
	if err != nil {
		return resp, err
	}

	resp = models.ParsedNodesFilters2{}

	for _, val := range nodes.Total {
		total, ok := val["total"]
		if !ok {
			continue
		}

		Total, ok := total.(float64)
		if !ok {
			continue
		}
		resp.Total += Total
	}

	resp.Products = nodes.Query
	resp.Page = page

	return resp, nil
}

// Метод get
func (service *NodesService) GetNodesOfCategoryByBrand(categoryName string, brandName string, offset int, page int) (models.ParsedNodesFilters2, error) {
	var resp models.ParsedNodesFilters2

	nodes, err := service.Repo.GetNodesOfCategoryByBrand(categoryName, brandName, offset, page)
	if err != nil {
		return resp, err
	}

	resp = models.ParsedNodesFilters2{}

	for _, val := range nodes.Total {
		total, ok := val["total"]
		if !ok {
			continue
		}

		Total, ok := total.(float64)
		if !ok {
			continue
		}
		resp.Total += Total
	}

	resp.Products = nodes.Query
	resp.Page = page

	return resp, nil
}

// Метод get
func (service *NodesService) GetNodesByBrand(brandName string, offset int, page int) (models.ParsedBrands2, error) {
	var resp models.ParsedBrands2

	nodes, err := service.Repo.GetNodesByBrand(brandName, offset, page)
	if err != nil {
		return resp, err
	}

	resp = models.ParsedBrands2{}

	for _, val := range nodes.Total {
		total, ok := val["total"]
		if !ok {
			continue
		}

		Total, ok := total.(float64)
		if !ok {
			continue
		}
		resp.Total += Total
	}

	resp.Products = nodes.Query
	resp.Page = page

	return resp, nil
}
