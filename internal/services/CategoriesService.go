package services

import (
	"RestAPI/internal/database/repositories"
	"RestAPI/internal/models"
	"fmt"
	"github.com/pkg/errors"
)

// Сервис для элементов
type CategoriesService struct {
	Repo *repositories.CategoriesRepository
}

// Метод get
func (service *CategoriesService) GetAllCategories() ([]string, error) {
	//var parsedCategories2 models.ParsedCategories2
	//for _, item := range parsedCategories2.Query {
	//	name, ok := item["name"].(string)
	//	if !ok {
	//		return models.ParsedCategories2{}, fmt.Errorf("name field is not a string")
	//	}
	//	parsedCategories2.Query = append(parsedCategories2.Query, name)
	//}
	//
	//return parsedCategories2, nil

	var parsedCategories models.ParsedCategories

	if len(parsedCategories.Query) == 0 {
		return nil, errors.New("parsedCategories.Query is empty")
	}

	var names []string
	for _, item := range parsedCategories.Query {
		name, ok := item["name"].(string)
		if !ok {
			return nil, fmt.Errorf("name field is not a string")
		}
		names = append(names, name)
	}

	return names, nil
}
