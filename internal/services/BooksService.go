package services

import (
	"RestAPI/internal/database/repositories"
)

// Сервис для элементов
type BooksService struct {
	Repo *repositories.BooksRepository
}

// Метод get
func (service *BooksService) GetAllBooks(page int, offset int) ([]map[string]interface{}, error) {
	//  логика для получения всех книг:
	books, err := service.Repo.GetAllBooks(page, offset)
	if err != nil {
		return nil, err
	}
	return books, nil
}
