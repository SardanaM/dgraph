package services

import (
	"RestAPI/internal/database/repositories"
)

// Сервис для элементов
type MedicineService struct {
	Repo *repositories.MedicineRepository
}

// Метод get
func (service *MedicineService) GetAllMedicine(page int, offset int) ([]map[string]interface{}, error) {
	//  логика для получения списков лекарств и БАДов:
	medicine, err := service.Repo.GetAllMedicine(page, offset)
	if err != nil {
		return nil, err
	}
	return medicine, nil
}
