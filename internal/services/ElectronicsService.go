package services

import (
	"RestAPI/internal/database/repositories"
)

// Сервис для элементов
type ElectronicsService struct {
	Repo *repositories.ElectronicsRepository
}

// Метод get
func (service *ElectronicsService) GetAllElectronics(categoryName string) (interface{}, error) {
	//  логика для получения всей электроники определенной категории:
	electroniсs, err := service.Repo.GetAllElectronics(categoryName)
	if err != nil {
		return nil, err
	}
	return electroniсs, nil
}
